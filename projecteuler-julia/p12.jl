function numdivisors(n)
    i = 1
    result = 0
    while i * i < n
        if n % i == 0
            result += 2
        end
        i += 1
    end
    if i * i == n
        result += 1
    end
    result
end

function solve()
    n = 1
    while true
        a = div(n * (n + 1), 2)
        500 < numdivisors(a) && return a
        n += 1
    end
end

solve() |> println
