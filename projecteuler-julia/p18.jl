a = map(i -> parse.(Int, split(i)), open(readlines, "data/input18.txt"))
result = reduce((a1, a2) -> max.(a1[2:end], a1[1:(end - 1)]) .+ a2, reverse(a))
println(result[1])
