function sumDivisors(n)
    result = 0
    i = 1
    while i * i < n
        if n % i == 0
            result += i + div(n, i)
        end
        i += 1
    end
    if i * i == n
        result += i
    end
    result - n
end

a = filter(n -> n < sumDivisors(n), 1:28123)
b = fill(true, 28123)
b[filter(n -> n <= 28123, a .+ a')] .= false
println(sum(filter(n -> b[n], 1:28123)))
