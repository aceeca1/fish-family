f(n) = n % 2 == 0 ? div(n, 2) : 3n + 1
len = fill(0, 1000000)
len[1] = 1

function solve(n)
    n <= 1000000 && len[n] != 0 && return len[n]
    result = solve(f(n)) + 1
    if n <= 1000000
        len[n] = result
    end
    result
end

argmax(solve.(1:1000000)) |> println
