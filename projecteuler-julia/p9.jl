for a in 1:1000
    s1 = 1000 - a
    s2 = a * a
    if s2 % s1 == 0
        b2 = s1 - div(s2, s1)
        if 0 <= b2 && b2 % 2 == 0
            b = div(b2, 2)
            c = s1 - b
            global result = a * b * c
            break
        end
    end
end
println(result)
