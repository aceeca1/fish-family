filter(1:(6 * 9 ^ 5)) do n
    n == sum(parse.(Int, split(string(n), "")) .^ 5)
end |> sum |> (x -> x - 1) |> println
