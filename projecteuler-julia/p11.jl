using DelimitedFiles
a = DelimitedFiles.readdlm("data/input11.txt", Int)
result = 0

function getProduct(x, y, xDelta, yDelta)
    product = 1
    for i in 1:4
        1 <= x <= 20 && 1 <= y <= 20 || return 0
        product *= a[x, y]
        x += xDelta
        y += yDelta
    end
    product
end

for i in 1:20
    for j in 1:20
        a1 = getProduct(i, j, 0, 1)
        a2 = getProduct(i, j, 1, 0)
        a3 = getProduct(i, j, 1, 1)
        a4 = getProduct(i, j, 1, -1)
        am = max(a1, a2, a3, a4)
        if result < am 
            global result = am
        end
    end
end
println(result)
