function cycleLen(n)
    remain = 1
    index = fill(0, n)
    now = 1
    while true
        remain == 0 && return 0
        index[remain] != 0 && return now - index[remain]
        index[remain] = now
        remain = remain * 10 % n
        now += 1
    end
end
argmax(cycleLen.(1:1000)) |> println
