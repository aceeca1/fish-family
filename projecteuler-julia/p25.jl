using Combinatorics
n = 1
while length(string(Combinatorics.fibonaccinum(n))) < 1000
    global n += 1
end
println(n)
