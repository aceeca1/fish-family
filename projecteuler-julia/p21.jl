function sumDivisors(n)
    result = 0
    i = 1
    while i * i < n
        if n % i == 0
            result += i + div(n, i)
        end
        i += 1
    end
    if i * i == n
        result += i
    end
    result - n
end

function amicable(n)
    m = sumDivisors(n)
    m != n && n == sumDivisors(m)
end

filter(amicable, 1:10000) |> sum |> println
