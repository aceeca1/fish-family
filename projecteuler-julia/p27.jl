using Primes
maxLen = 0
result = 0
for b in filter(Primes.isprime, 2:1000)
    for a in -999:999
        n = 0
        while Primes.isprime(n * n + a * n + b)
            n += 1
        end
        if result < n
            global result = n
            global arg = a * b
        end
    end
end
println(arg)
