s = open(readlines, "data/input8.txt") |> join
map(1:(length(s) - 12)) do i
    prod(parse.(Int, split(s[i:(i + 12)], "")))
end |> maximum |> println
