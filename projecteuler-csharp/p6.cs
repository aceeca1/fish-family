using System;

class P6 {
    public static void Main() {
        var answer1 = 0;
        var answer2 = 0;
        for (int i = 1; i <= 100; ++i) {
            answer1 += i * i;
            answer2 += i;
        }
        Console.WriteLine(answer2 * answer2 - answer1);
    }
}
