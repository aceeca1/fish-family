using System;

class P11 {
    public static int[,] A = new int[20, 20];

    public static int GetProduct(int x, int y, int xDelta, int yDelta) {
        var answer = 1;
        for (int i = 0; i < 4; ++i) {
            if (!(0 <= x && x < 20)) return 0;
            if (!(0 <= y && y < 20)) return 0;
            answer *= A[x, y];
            x += xDelta;
            y += yDelta;
        }
        return answer;
    }

    public static void Main() {
        using (var file = new System.IO.StreamReader("data/input11.txt")) {
            for (int i = 0; i < 20; ++i) {
                var ai = file.ReadLine().Split();
                for (int j = 0; j < 20; ++j) A[i, j] = int.Parse(ai[j]);
            }
        }
        var answer = 0;
        for (int i = 0; i < 20; ++i) {
            for (int j = 0; j < 20; ++j) {
                var p1 = GetProduct(i, j, 0, 1);
                if (answer < p1) answer = p1;
                var p2 = GetProduct(i, j, 1, 0);
                if (answer < p2) answer = p2;
                var p3 = GetProduct(i, j, 1, 1);
                if (answer < p3) answer = p3;
                var p4 = GetProduct(i, j, 1, -1);
                if (answer < p4) answer = p4;
            }
        }
        Console.WriteLine(answer);
    }
}
