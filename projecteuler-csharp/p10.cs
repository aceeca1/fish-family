using System;

class P7 {
    public static void Main() {
        var m = 2000000;
        var nonPrime = new bool[m + 1];
        for (long i = 2; i <= m; ++i) {
            if (!nonPrime[i]) {
                for (long j = i * i; j <= m; j += i) {
                    nonPrime[j] = true;
                }
            }
        }
        var answer = 0L;
        for (int i = 2; i <= m; ++i) {
            if (!nonPrime[i]) answer += i;
        }
        Console.WriteLine(answer);
    }
}
