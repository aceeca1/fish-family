using System;

class P5 {
    public static long GCD(long a1, long a2) {
        if (a2 == 0) return a1;
        return GCD(a2, a1 % a2);
    }

    public static long LCM(long a1, long a2) {
        return a1 / GCD(a1, a2) * a2;
    }

    public static void Main() {
        var answer = 1L;
        for (long i = 2L; i <= 20L; ++i) {
            answer = LCM(answer, i);
        }
        Console.WriteLine(answer);
    }
}
