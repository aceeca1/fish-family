using System;

class P7 {
    public static void Main() {
        var m = 200000;
        var nonPrime = new bool[m + 1];
        for (long i = 2; i <= m; ++i) {
            if (!nonPrime[i]) {
                for (long j = i * i; j <= m; j += i) {
                    nonPrime[j] = true;
                }
            }
        }
        var n = 10001;
        var answer = 0;
        for (int i = 2; i <= m; ++i) {
            if (!nonPrime[i]) --n;
            if (n == 0) {
                answer = i;
                break;
            }
        }
        Console.WriteLine(answer);
    }
}
