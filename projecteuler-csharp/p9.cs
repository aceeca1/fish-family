using System;

class P9 {
    public static void Main() {
        var answer = 0;
        for (int a = 1; a <= 999; ++a) {
            var s1 = 1000 - a;
            var s2 = a * a;
            if (s2 % s1 != 0) continue;
            var b2 = s1 - s2 / s1;
            if (b2 % 2 != 0) continue;
            var b = b2 / 2;
            if (b <= 0) continue;
            var c = s1 - b;
            answer = a * b * c;
            break;
        }
        Console.WriteLine(answer);
    }
}
