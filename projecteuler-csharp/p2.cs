using System;

class P2 {
    public static void Main() {
        var answer = 0;
        var a1 = 1;
        var a2 = 2;
        while (a1 <= 4000000) {
            if (a1 % 2 == 0) answer += a1;
            var a3 = a1 + a2;
            a1 = a2;
            a2 = a3;
        }
        Console.WriteLine(answer);
    }
}
