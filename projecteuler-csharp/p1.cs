using System;

class P1 {
    public static void Main() {
        var answer = 0;
        for (int i = 1; i < 1000; ++i)
            if (i % 3 == 0 || i % 5 == 0)
                answer += i;
        Console.WriteLine(answer);
    }
}
