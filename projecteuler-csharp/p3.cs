using System;

class P3 {
    public static void Main() {
        var n = 600851475143L;
        var answer = 0L;
        for (long i = 2L; i * i <= n; ++i) {
            while (n % i == 0L) {
                if (answer < i) answer = i;
                n /= i;
            }
        }
        if (n != 1L) {
            if (answer < n) answer = n;
        }
        Console.WriteLine(answer);
    }
}
