using System;

class P4 {
    public static void Main() {
        var answer = 0;
        for (int i = 100; i < 1000; ++i) {
            for (int j = 100; j < 1000; ++j) {
                var k = i * j;
                var s = k.ToString();
                var r = s.ToCharArray();
                Array.Reverse(r);
                if (s == new string(r)) {
                    if (answer < k) answer = k;
                }
            }
        }
        Console.WriteLine(answer);
    }
}
