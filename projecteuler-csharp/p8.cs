using System;
using System.IO;
using System.Text;

class P8 {
    public static void Main() {
        var s = new StringBuilder();
        using (var file = new StreamReader("data/input8.txt")) {
            for (int i = 0; i < 20; ++i) s.Append(file.ReadLine());
        }
        var answer = 0L;
        for (int i = 0; i < s.Length - 12; ++i) {
            var product = 1L;
            for (int j = i; j < i + 13; ++j) product *= s[j] - '0';
            if (answer < product) answer = product;
        }
        Console.WriteLine(answer);
    }
}
