isSpecial = function(n) {
    s = as.character(n)
    s = strsplit(s, split='')[[1]]
    n == sum(as.integer(s) ^ 5)
}
sum(which(sapply(1:(6 * 9 ^ 5), isSpecial))) - 1
